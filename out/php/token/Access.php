<?php

class Access
{
    private static $connection;

    public static function getAccess($email, $password)
    {
        self::$connection = Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');

        // Сделать это независимо от таблицы через класс таблицы с пользователями
        $sql = "SELECT * FROM `users` WHERE `email` = '$email'";
        $user = ($user_db = mysqli_query(self::$connection, $sql)) ? mysqli_fetch_array($user_db) : null;

        if ($user === null) {
            Connection::closeConnection(self::$connection);
            echo json_encode(array(
                "errno" => "au700",
                "message" => "Access denied"
            ));
            return false;
        }

        if (Security::validatePassword($password, $user['password'], $user['salt'])) {
            $access_token = Token::getAccessToken($user);
            $refresh_token = Token::getRefreshToken($user);

            if ($access_token && $refresh_token) {
                Connection::closeConnection(self::$connection);
                echo json_encode(array(
                    "errno" => 0,
                    "message" => "You have successfully logged in",
                    "access" => $access_token,
                    "refresh" => $refresh_token
                ));
                return true ;
            }
            else {
                Connection::closeConnection(self::$connection);
                echo json_encode(array(
                    "errno" => "au702",
                    "message" => "Token error"
                ));
                return false;
            }
        }
        else {
            Connection::closeConnection(self::$connection);
            echo json_encode(array(
                "errno" => "au701",
                "message" => "Access denied"
            ));
            return false;
        }
    }

    public static function getRefresh($refresh_token)
    {
        self::$connection = Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');

        $sql = "SELECT * FROM `admins` WHERE `refresh_token` = '$refresh_token'";
        $user = ($user_db = mysqli_query(self::$connection, $sql)) ? mysqli_fetch_array($user_db) : null;

        Connection::closeConnection(self::$connection);

        if ($user) {
            $refresh = explode(".", $refresh_token);
            $payload = json_decode(base64_decode($refresh[1]));

            if (Token::checkTokenTime($payload->exp)) {
                $access_token = Token::getAccessToken($user);
                $refresh_token = Token::getRefreshToken($user);

                if ($access_token && $refresh_token)
                    echo json_encode(array(
                        "errno" => 0,
                        "message" => "Token successfully updated",
                        "access" => $access_token,
                        "refresh" => $refresh_token
                    ));
                else
                    echo json_encode(array(
                        "errno" => "rf702",
                        "message" => "Token error"
                    ));
            }
            else {
                echo json_encode(array(
                    "errno" => "rf701",
                    "message" => "Access denied"
                ));
            }
        }
        else {
            echo json_encode(array(
                "errno" => "rf700",
                "message" => "Access denied"
            ));
        }
    }

    public static function checkTokens($access_token, $refresh_token)
    {
        $access = explode(".", $access_token);
        $access_payload = json_decode(base64_decode($access[1]));

        if (Token::checkTokenTime($access_payload->exp))
            echo json_encode([
                'status' => 'aTrue'
            ]);
        else
            echo json_encode([
                'status' => 'aFalse'
            ]);

        $refresh = explode(".", $refresh_token);
        $refresh_payload = json_decode(base64_decode($refresh[1]));

        if (Token::checkTokenTime($refresh_payload->exp))
            echo json_encode([
                'status' => 'rTrue'
            ]);
        else
            echo json_encode([
                'status' => 'rFalse'
            ]);
    }
}