<?php


class ClienteleTable
{
    private static $tableName = "clientele";

    /**
     * @param $client Client
     * @return bool
     */
    public static function insert($client)
    {
        $data = [
            'phone' => $client->getPhone(),
            'ip' => $client->getIp(),
            'useragent' => $client->getUserAgent()
        ];

        $sql = Database::insertRequest(self::$tableName, $data);

        return Database::query($sql);
    }
}