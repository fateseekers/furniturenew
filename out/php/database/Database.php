<?php


class Database
{
    /**
     *  Получение массива данных по соединению и SQL-запросу
     *
     * @param $connection   - соединение с базой данных
     * @param $sql          - запрос к базе даных
     * @return array|null   - массив данных
     */
    public static function getQueryArray($connection, $sql)
    {
        $res = array();

        if ($query = mysqli_query($connection, $sql)) {
            while ($arr = mysqli_fetch_assoc($query)) {
                $res[] = $arr;
            }

            return $res;
        }
        else
            return null;
    }

    public static function query($sql)
    {
        $connection = Connection::getConnection();

        return mysqli_query($connection, $sql) or die(mysqli_error($connection));
    }

    /**
     * @param $tableName
     * @param $data array
     * @return string
     */
    public static function insertRequest($tableName, $data)
    {
        $count = 0;
        $sql = "";

        $sql .= "INSERT INTO `$tableName` (";

        foreach ($data as $column => $value) {
            if ($count)
                $sql .= ", `$column`";
            else
                $sql .= "`$column`";

            $count = 1;
        }

        $sql .= ") VALUES (";
        $count = 0;

        foreach ($data as $column => $value) {
            if ($count)
                $sql .= ", '$value'";
            else
                $sql .= "'$value'";

            $count = 1;
        }

        $sql .= ")";

        return $sql;
    }
}