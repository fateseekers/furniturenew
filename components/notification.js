import React, {Component} from 'react';
import {MDBIcon} from "mdbreact";

export default class Notification extends Component{
    static getInitialProps({type: type, content:content}){
        console.log(type, content)
        return({type: type, content:content })
    }
    render() {
        switch(this.props.type){
            case "error":
                return (
                    <div className="notification danger-color">
                        <div className="notification__icon">
                            <MDBIcon icon="exclamation-circle"/>
                        </div>
                        <div className="notification__heading">
                            <h5 className="h5-responsive mb-0">{this.props.content}</h5>
                        </div>
                    </div>
            )
            case"success":
                return (
                    <div className="notification success-color">
                        <div className="notification__icon">
                            <MDBIcon icon="check-circle"/>
                        </div>
                        <div className="notification__heading">
                            <h5 className="h5-responsive mb-0">{this.props.content}</h5>
                        </div>
                    </div>
                )
        }
    }
}