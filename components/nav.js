import React from 'react'
import Link from 'next/link'
import OrderBtn from './orderBtn'
import { MDBNav, MDBBtn, MDBIcon } from "mdbreact";

class Nav extends React.Component {
  static getInitialProps({toggle: toggle}){
    return ({toggle: toggle})
  }
  render() {
    return (
      <header className="header">
        <MDBNav className="header py-2">
          <div className="container">
            <div className="row">
              <div className="col-12 d-flex align-items-center justify-content-between">
                <div className="header__brand d-flex flex-column color-white">
                  <h1 className="h3-responsive mb-0">Roomniture</h1>
                  <h2 className="h5-responsive">Мебель на заказ</h2>
                </div>
                <div className="header__contacts d-flex align-items-center">
                  <Link href="tel:+7 900 475 6568"><a className="h5-responsive d-none d-md-flex px-2">+7 900 475 6568</a></Link>
                  <OrderBtn class="mobile__btn" btn_text={
                    <>
                      <span className="d-none d-md-flex white-text">Заказать звонок</span>
                      <span className="d-flex d-md-none white-text">
                        <MDBIcon icon="phone" />
                      </span>
                    </>
                  } toggle={this.props.toggle}/>
                </div>
              </div>
            </div>
          </div>
        </MDBNav>
      </header>
    )
  }
}


export default Nav
