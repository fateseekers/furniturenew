import React, {Component} from 'react';
import {MDBBtn, MDBIcon} from "mdbreact";
import InputMask from 'react-input-mask';
import Notification from "./notification";
import * as ReactDOM from "react-dom";

export default class PhoneInput extends Component{
    constructor(props){
        super(props);

        this.state = {
            phone: "",
            errors: []
        };

        this.checkPhone = this.checkPhone.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    checkPhone(e){
        console.log(e.target.value);
        this.setState({phone: e.target.value})
    }

    appendNotification(type, content){
        let notifications = document.getElementsByClassName('notifications')[0];
        ReactDOM.render(<Notification type={type} content={content}/>, notifications)
        setTimeout(()=>{
            ReactDOM.unmountComponentAtNode(notifications)
        }, 4000)
    }

    async onSubmit(e){

        let errors = [];
        e.preventDefault()
        if(this.state.phone.indexOf("_") >= 0){
            errors.push("Вы не дописали телефон");
            await this.setState({errors: errors});
            console.log(this.state.errors)
            this.appendNotification("error", this.state.errors[0])

        } else if(this.state.phone === ""){
            errors.push("Вы не ввели телефон");
            await this.setState({errors: errors});
            console.log(this.state.errors)
            this.appendNotification("error", this.state.errors[0])
        } else {
            this.appendNotification("success", "Все введено верно")
        }
    }

    render(){
        return(
            <form className="form-group d-flex justify-content-center" onSubmit={this.onSubmit}>
                <InputMask type="text" mask="+7(999)-999-99-99" maskChar="_" className="form-control phone_block__input text-center" onChange={this.checkPhone} value={this.state.phone}/>
                <MDBBtn type="submit" gradient="aqua" className="white-text m-0 d-none d-md-flex justify-content-center"> Отправить </MDBBtn>
                <MDBBtn type="submit" gradient="aqua" className="white-text m-0 d-flex d-md-none justify-content-center" size="sm">
                    <MDBIcon icon="angle-right" size="2x" />
                </MDBBtn>
            </form>
        )
    }
}