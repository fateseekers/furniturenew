import React, {Component} from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBFooter, MDBBtn, MDBIcon } from "mdbreact";

export default class Footer extends Component{
    render() {
        return(
            <>
            <MDBFooter color="aqua-gradient">
                <MDBContainer className="text-center text-md-left">
                    <MDBRow className="py-5">
                        <MDBCol md="3" className="d-flex">
                            <MDBBtn className="social_btn" color="" href="https://vk.com/id151207594" target="_blank">
                                <MDBIcon fab icon="vk" />
                            </MDBBtn>
                            <MDBBtn className="social_btn" color="">
                                <MDBIcon fab icon="instagram" />
                            </MDBBtn>
                            <MDBBtn className="social_btn" color="">
                                <MDBIcon fab icon="facebook-f" />
                            </MDBBtn>
                        </MDBCol>
                        <MDBCol md="9">
                            <h5>Контакты: </h5>
                                <p>Телефон:
                                    <a className="ml-2 mb-0" href="tel:(+79004756568)"><u>+7 900 475 6568</u></a>
                                </p>
                                <p>Почта:
                                    <a className="ml-2" href="mailto:fateseekers@mail.ru"><u>fateseekers@mail.ru</u></a>
                                </p>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <div className="footer-copyright text-center py-3">
                    <MDBContainer>
                        &copy; {new Date().getFullYear()} Copyright: Все права защищены<br />
                        Запрещено любое копирование материалов ресурса без письменного согласия владельца — ООО "...".
                    </MDBContainer>
                </div>
            </MDBFooter>
            <div className="notifications">

            </div>
        </>
        )
    }
}