import React, { Component } from 'react';

import { MDBBtn, MDBIcon } from 'mdbreact'
import PhoneInput from "./phoneInput";

export default class PhoneBlock extends Component {
    render() {
        return (
            <section className="py-5 phone_block img-gradient text-center">
                <div className="container">
                    <div className="row">
                        <div className="col-12 d-flex flex-column">
                            <h5 className="h5-responsive white-text font-weight-bold">Оставьте свой номер, и мы Вам перезвоним!</h5>
                            <p className="white-text">Мы перезвоним Вам в рабочее время, и обсудим детали заказа</p>
                            <PhoneInput />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}