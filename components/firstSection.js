import React, { Component } from 'react';
import OrderBtn from './orderBtn';

export default class FirstSection extends Component{
    static getInitialProps({toggle: toggle}){
        return ({toggle: toggle})
    }
    render(){
        return(
            <section className="first__section py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="first__section__bg d-flex flex-column align-items-center text-center">
                                <h5 className="h3-responsive first__section__heading text-white">Индивидуальная мебель на заказ</h5>
                                <OrderBtn class={"order__btn"} btn_text={"Оформить заказ"} toggle={this.props.toggle}/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}