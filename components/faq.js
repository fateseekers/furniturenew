import React, {Component} from 'react';

export default class Faq extends Component{
    constructor(props){
        super(props)
    }

    onToggle(e){
        let target = document.getElementById(e.target.dataset.target);
        if(target.classList.contains("show")){
            target.classList.remove("show")
        } else {
            target.classList.add("show")
        }
    }

    render() {
        return(
            <section className="py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div id="accordion">
                                <div className="card">
                                    <div className="card-header aqua-gradient" id="headingOne">
                                        <h5 className="mb-0">
                                            <button className="btn btn-link button__collapse" data-toggle="collapse"
                                                    data-target="collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne" onClick={this.onToggle}>
                                                Как вы узнаете, что я хочу?
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" className="collapse" aria-labelledby="headingOne"
                                         data-parent="#accordion">
                                        <div className="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                            dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                            tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                            assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                            vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                                            synth nesciunt you probably haven't heard of them accusamus labore
                                            sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header aqua-gradient" id="headingTwo">
                                        <h5 className="mb-0">
                                            <button className="btn btn-link collapsed button__collapse" data-toggle="collapse"
                                                    data-target="collapseTwo" aria-expanded="false"
                                                    aria-controls="collapseTwo" onClick={this.onToggle}>
                                                Как вы узнаете, что я хочу?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"
                                         data-parent="#accordion">
                                        <div className="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                            dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                            tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                            assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                            vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                                            synth nesciunt you probably haven't heard of them accusamus labore
                                            sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header aqua-gradient" id="headingThree">
                                        <h5 className="mb-0">
                                            <button className="btn btn-link collapsed button__collapse" data-toggle="collapse"
                                                    data-target="collapseThree" aria-expanded="false"
                                                    aria-controls="collapseThree" onClick={this.onToggle}>
                                                Как вы узнаете, что я хочу?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree"
                                         data-parent="#accordion">
                                        <div className="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                            richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                            dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                            tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                                            assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                            vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic
                                            synth nesciunt you probably haven't heard of them accusamus labore
                                            sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}