import React, { Component } from 'react';
import {MDBBtn} from 'mdbreact'

export default class OrderBtn extends Component{
    static async getInitialProps({ class: class_text, btn_text: string_text, toggle: toggle}) {
        return ({class: class_text, btn_text: string_text, toggle: toggle});
    }
    render(){
        return(
            <MDBBtn gradient="aqua" className={this.props.class + " white-text m-0"} onClick={this.props.toggle}> {this.props.btn_text} </MDBBtn>
        )
    }
}