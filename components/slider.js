import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import { MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle } from 'mdbreact';

export default class Slider extends Component {
    render() {
        const params = {
            effect: 'coverflow',
            grabCursor: true,
            slidesPerView: 'auto',
            loop: true,
            centeredSlides: true,
            coverflowEffect: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: false
            }
        }

        return (
            <section className="py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-12 mb-4">
                            <h5 className="h5-responsive text-center font-weight-bold">Какую мебель мы делаем на заказ?</h5>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <Swiper {...params}>
                                <div className="swiper-slider">
                                    <MDBCard>
                                        <MDBCardImage className="img-fluid" src="/static/img/slider/1.jpg" waves />
                                        <MDBCardBody>
                                            <MDBCardTitle className="h5-responsive">Мебель для гостинной</MDBCardTitle>
                                        </MDBCardBody>
                                    </MDBCard>
                                </div>
                                <div className="swiper-slider">
                                    <MDBCard>
                                        <MDBCardImage className="img-fluid" src="/static/img/slider/2.webp" waves />
                                        <MDBCardBody>
                                            <MDBCardTitle className="h5-responsive">Мебель для ванной</MDBCardTitle>
                                        </MDBCardBody>
                                    </MDBCard>
                                </div>
                                <div className="swiper-slider">
                                    <MDBCard>
                                        <MDBCardImage className="img-fluid" src="/static/img/slider/3.jpg" waves />
                                        <MDBCardBody>
                                            <MDBCardTitle className="h5-responsive">Мебель для спальни</MDBCardTitle>
                                        </MDBCardBody>
                                    </MDBCard>
                                </div>
                                <div className="swiper-slider">
                                    <MDBCard>
                                        <MDBCardImage className="img-fluid" src="/static/img/slider/4.jpg" waves />
                                        <MDBCardBody>
                                            <MDBCardTitle className="h5-responsive">Мебель для кухни</MDBCardTitle>
                                        </MDBCardBody>
                                    </MDBCard>
                                </div>
                                <div className="swiper-slider">
                                    <MDBCard>
                                        <MDBCardImage className="img-fluid" src="/static/img/slider/5.jpg" waves />
                                        <MDBCardBody>
                                            <MDBCardTitle className="h5-responsive">Мебель для детской</MDBCardTitle>
                                        </MDBCardBody>
                                    </MDBCard>
                                </div>
                            </Swiper>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}