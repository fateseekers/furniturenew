import  React, {Component} from 'react';
import { MDBModal, MDBModalBody, MDBModalHeader, MDBBtn } from "mdbreact";
import PhoneInput from "./phoneInput";

export default class Modal extends Component{
    static getInitialProps({modal: modal, toggle: toggle}){
        return ({modal: modal, toggle: toggle})
    }

    render() {
        return(
            <MDBModal isOpen={this.props.modal} toggle={this.props.toggle} centered>
                <MDBModalHeader toggle={this.props.toggle} className="h5-responsive">Оставьте заявку, и мы Вам перезвоним!</MDBModalHeader>
                <MDBModalBody className="d-flex modal__heading">
                    <PhoneInput />
                </MDBModalBody>
            </MDBModal>
        )
    }
}