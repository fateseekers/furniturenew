import React, { Component } from 'react';

export default class howWeWork extends Component {
    render() {
        return (
            <section className="py-5">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12 d-flex flex-column">
                                    <h5 className="text-center mb-4 h5-responsive font-weight-bold">Как мы работаем?</h5>
                                    <div className="ourwork">
                                        <span className="number">1</span>
                                        <p className="description">Обсуждаем с Вами какую мебель вы хотите заказать</p>
                                    </div>
                                    <div className="ourwork">
                                        <span className="number">2</span>
                                        <p className="description">Обсуждаем с Вами какую мебель вы хотите заказать</p>
                                    </div>
                                    <div className="ourwork">
                                        <span className="number">3</span>
                                        <p className="description">Обсуждаем с Вами какую мебель вы хотите заказать</p>
                                    </div>
                                    <div className="ourwork">
                                        <span className="number"> 4</span>
                                        <p className="description"> Обсуждаем с Вами какую мебель вы хотите заказать</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}