const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const withPlugins = require("next-compose-plugins");
const withCSS = require('@zeit/next-css');
const withFonts = require('next-fonts');
const withImages = require('next-images');


module.exports = withPlugins([withCSS, withFonts, withImages], {
        webpack: (config, { isServer }) => {
            config.plugins.push(
                new CopyWebpackPlugin([
                    {
                        from: path.join(__dirname, 'php/'),
                        to: path.join(__dirname, 'out/')
                    },
                    {
                        from: path.join(__dirname, 'app/'),
                        to: path.join(__dirname, 'out/php/')
                    },
                    {
                        from: path.join(__dirname, 'vendor/'),
                        to: path.join(__dirname, 'out/vendor/')
                    }
                ])
            );
            return config;
        }
    }
)

