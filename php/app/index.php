<?php

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

require '../../../app/autoload.php';



/* ========== Р О У Т И Н Г ========== */

$app = new \Slim\App();
$app->post('/phone/add', 'phoneAdd');
$app->run();



/* ========== Д О П Ф У Н К Ц И И ========== */

function getGetData(ServerRequestInterface $request) {
    return $request->getQueryParams();
}

function getPostData(ServerRequestInterface $request) {
    $body = array_flip($request->getParsedBody());
    return json_decode($body[''], true);
}



/* ========== О Б Р А Б О Т Ч И К И ========== */

function phoneAdd(ServerRequestInterface $request, ResponseInterface $response) {
    $data = getPostData($request);

    $phone = $data['phone'];
    $ip = $request->getServerParams()['REMOTE_ADDR'];
    $useragent = $request->getServerParams()['HTTP_USER_AGENT'];

    ClientController::add($phone, $ip, $useragent);
}