<?php

class Token
{
    const ACCESS_TIME = 172800;
    const REFRESH_TIME = 518400;

    private static $key;
    private static $time;
    private static $connection;

    public static function getAccessToken($user)
    {
        self::$connection = Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
        self::$key = Security::getSecretKey($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
        self::$time = time();
        $access_time = self::$time + self::ACCESS_TIME;

        $header = array(
            "alg" => "HS256",
            "typ" => "jwt"
        );
        $payload = array(
            "uid" => $user['id'],
            "email" => $user['email'],
            "name" => $user['name'],
            "surname" => $user['surname'],
            "role" => $user['role'],
            "iat" => self::$time,
            "exp" => self::$time + $access_time
        );

        $header = json_encode($header);
        $payload = json_encode($payload);

        $unsigned_token = base64_encode($header) . "." . base64_encode($payload);
        $signature = hash_hmac("sha256", $unsigned_token, self::$key);
        $token = $unsigned_token . "." . $signature;

        UsersTable::setAccessToken($user['id'], $token, $access_time, self::$connection);
        Connection::closeConnection(self::$connection);

        return $token;
    }

    public static function getRefreshToken($user)
    {
        self::$connection = Connection::getConnectionFromFile($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
        self::$key = Security::getSecretKey($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
        self::$time = time();
        $refresh_time = self::$time + self::REFRESH_TIME;

        $header = array(
            "alg" => "HS256",
            "typ" => "jwt"
        );
        $payload = array(
            "uid" => $user['id'],
            "sub" => "Refresh token for " . $user['email'],
            "iat" => self::$time,
            "exp" => self::$time + $refresh_time
        );

        $header = json_encode($header);
        $payload = json_encode($payload);

        $unsigned_token = base64_encode($header) . "." . base64_encode($payload);
        $signature = hash_hmac("sha256", $unsigned_token, self::$key);
        $token = $unsigned_token . "." . $signature;

        UsersTable::setRefreshToken($user['id'], $token, $refresh_time, self::$connection);
        Connection::closeConnection(self::$connection);

        return $token;
    }

    public static function checkSignature(array $token)
    {
        self::$key = Security::getSecretKey($_SERVER['DOCUMENT_ROOT'] . '/../php/.ini');
        $signature = hash_hmac("sha256", $token[0] . "." . $token[1], self::$key);

        return ($signature === $token[2]) ? true : false;
    }

    public static function checkTokenTime($time)
    {
        return ($time > time()) ? true : false;
    }

}