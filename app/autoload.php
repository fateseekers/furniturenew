<?php

require_once "config.php";

require ROOT . "vendor/autoload.php";
require_once "Connector.php";

Connector::requireFolder(APP . "auth");
Connector::requireFolder(APP . "controllers");
Connector::requireFolder(APP . "database");
Connector::requireFolder(APP . "model");
Connector::requireFolder(APP . "security");
Connector::requireFolder(APP . "tables");
Connector::requireFolder(APP . "test");
Connector::requireFolder(APP . "token");