<?php


class Client
{
    private $phone;
    private $ip;
    private $userAgent;

    /**
     * Client constructor.
     * @param $phone
     * @param $ip
     * @param $userAgent
     */
    public function __construct($phone, $ip, $userAgent)
    {
        $this->phone = $phone;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return mixed
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }


}