import React from 'react'
import Head from '../components/head'
import Nav from '../components/nav'
import PhoneBlock from './../components/phoneBlock'
import FirstSection from './../components/firstSection'
import Individual from './../components/individual'
import AdvantagesSection from './../components/advantagesSection'
import HowWeWork from './../components/howWeWork'
import Slider from '../components/slider'
import Footer from '../components/footer'
import Faq from "../components/faq";
import Modal from "../components/modal"
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import 'react-id-swiper/lib/styles/css/swiper.css';
import '../style.css';
import Notification from "../components/notification";

class Home extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            modal: false
        }
        this.toggle = this.toggle.bind(this)
    }

    toggle(){
        this.setState({modal: !this.state.modal})
    }

    // static getInitialProps(){
    //     console.log(
    //         fetch('http://localhost:3000/static/app/phone/add')
    //     )
    // }

  render(){
    return(
      <>
        <Head title="Мебель на заказ" description="Мебель на заказ от компании Roomniture. Собственная сборка, выполнение мебели на заказ"/>
        <Nav toggle={this.toggle}/>
        <FirstSection toggle={this.toggle}/>
        <Individual />
        <PhoneBlock />
        <AdvantagesSection />
        <PhoneBlock />
        <HowWeWork />
        <Slider />
        <PhoneBlock />
        <Faq />
        <Modal modal={this.state.modal} toggle={this.toggle}/>
        <Footer />
      </>
    )
  }
}

export default Home
